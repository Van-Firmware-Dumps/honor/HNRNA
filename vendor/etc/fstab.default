# Copyright (c) 2019-2020 The Linux Foundation. All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted (subject to the limitations in the
# disclaimer below) provided that the following conditions are met:
#
#    * Redistributions of source code must retain the above copyright
#      notice, this list of conditions and the following disclaimer.
#
#    * Redistributions in binary form must reproduce the above
#      copyright notice, this list of conditions and the following
#      disclaimer in the documentation and/or other materials provided
#      with the distribution.
#
#    * Neither the name of The Linux Foundation nor the names of its
#      contributors may be used to endorse or promote products derived
#      from this software without specific prior written permission.
#
# NO EXPRESS OR IMPLIED LICENSES TO ANY PARTY'S PATENT RIGHTS ARE
# GRANTED BY THIS LICENSE. THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT
# HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED
# WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
# IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
# ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
# GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
# IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
# OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
# IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

# Android fstab file.
# The filesystem that contains the filesystem checker binary (typically /system) cannot
# specify MF_CHECK, and must come before any filesystems that do specify MF_CHECK

# <src>                                                 <mnt_point>            <type>  <mnt_flags and options>                            <fs_mgr_flags>
system                                                  /system                ext4    ro,barrier=1,discard                                 wait,slotselect,logical,first_stage_mount,avb
system                                                  /system                erofs   ro                                                   wait,slotselect,logical,first_stage_mount,avb
system_ext                                              /system_ext            ext4    ro,barrier=1,discard                                 wait,slotselect,logical,first_stage_mount,avb,nofail
system_ext                                              /system_ext            erofs   ro                                                   wait,slotselect,logical,first_stage_mount,avb,nofail
# product                                               /product               ext4    ro,barrier=1,discard                                 wait,slotselect,logical,first_stage_mount
vendor                                                  /vendor                ext4    ro,barrier=1,discard                                 wait,slotselect,logical,first_stage_mount,avb
vendor                                                  /vendor                erofs   ro                                                   wait,slotselect,logical,first_stage_mount,avb
odm                                                     /odm                   ext4    ro,barrier=1,discard                                 wait,slotselect,logical,first_stage_mount,avb
odm                                                     /odm                   erofs   ro                                                   wait,slotselect,logical,first_stage_mount,avb
cust                                                    /cust                  ext4    ro,barrier=1,discard                                 wait,slotselect,logical,first_stage_mount,avb,nofail
cust                                                    /cust                  erofs   ro                                                   wait,slotselect,logical,first_stage_mount,avb,nofail
hw_product                                               /product_h             ext4    ro,barrier=1,discard                                 wait,slotselect,logical,first_stage_mount,avb,nofail
hw_product                                               /product_h             erofs   ro                                                   wait,slotselect,logical,first_stage_mount,avb,nofail
preas                                                   /preas                 ext4    ro,barrier=1,discard                                 wait,slotselect,logical,first_stage_mount,avb,nofail
preas                                                   /preas                 erofs   ro                                                   wait,slotselect,logical,first_stage_mount,avb,nofail
preavs                                                  /preavs                ext4    ro,barrier=1,discard                                 wait,slotselect,logical,first_stage_mount,avb,nofail
preavs                                                  /preavs                erofs   ro                                                   wait,slotselect,logical,first_stage_mount,avb,nofail
version                                                 /version               ext4    ro,barrier=1,discard                                 wait,slotselect,logical,first_stage_mount,avb,nofail
version                                                 /version               erofs   ro                                                   wait,slotselect,logical,first_stage_mount,avb,nofail
preload                                                 /preload               ext4    ro,barrier=1,discard                                 wait,slotselect,logical,first_stage_mount,avb,nofail
preload                                                 /preload               erofs   ro                                                   wait,slotselect,logical,first_stage_mount,avb,nofail
/dev/block/by-name/metadata                             /metadata              ext4    noatime,nosuid,nodev,discard,commit=1                         wait,check,formattable,first_stage_mount
/dev/block/by-name/patch                                /patch_hn              erofs   ro                                                   wait,slotselect,nofail,first_stage_mount,avb_keys=/patch_avb_key
/dev/block/by-name/patch                                /patch_hn              ext4    ro,barrier=1,discard                                 wait,slotselect,nofail,first_stage_mount,avb_keys=/patch_avb_key
/dev/block/bootdevice/by-name/persist                   /mnt/vendor/persist    ext4    noatime,nosuid,nodev,barrier=1                       wait
/dev/block/bootdevice/by-name/userdata                  /data                  f2fs    noatime,nosuid,nodev,discard,inlinecrypt,reserve_root=32768,resgid=1065,fsync_mode=nobarrier    latemount,wait,check,fileencryption=aes-256-xts:aes-256-cts:v2+inlinecrypt_optimized+wrappedkey_v0,keydirectory=/metadata/vold/metadata_encryption,metadata_encryption=aes-256-xts:wrappedkey_v0,quota,reservedsize=128M,sysfs_path=/sys/devices/platform/soc/1d84000.ufshc,checkpoint=fs
/dev/block/by-name/misc                      /misc                  emmc    defaults                                             defaults
/devices/platform/soc/8804000.sdhci/mmc_host*           /storage/sdcard1       vfat    nosuid,nodev                                         wait,voldmanaged=sdcard1:auto
/devices/platform/soc/1da4000.ufshc_card/host*          /storage/sdcard1       vfat    nosuid,nodev                                         wait,voldmanaged=sdcard1:auto
/devices/platform/soc/*.ssusb/*.dwc3/xhci-hcd.*.auto*   /storage/usbotg        vfat    nosuid,nodev                                         wait,voldmanaged=usbotg:auto
/dev/block/bootdevice/by-name/modem                     /vendor/firmware_mnt   vfat    ro,shortname=lower,uid=1000,gid=1000,dmask=227,fmask=337,context=u:object_r:firmware_file:s0 wait,slotselect
/dev/block/bootdevice/by-name/dsp                       /vendor/dsp            ext4    ro,nosuid,nodev,barrier=1                            wait,slotselect
/dev/block/bootdevice/by-name/vm-bootsys              /vendor/vm-system      ext4    ro,nosuid,nodev,barrier=1                            wait,slotselect
/dev/block/bootdevice/by-name/bluetooth                 /vendor/bt_firmware    vfat    ro,shortname=lower,uid=1002,gid=3002,dmask=227,fmask=337,context=u:object_r:bt_firmware_file:s0 wait,slotselect
/dev/block/bootdevice/by-name/qmcs                      /mnt/vendor/qmcs       vfat    noatime,nosuid,nodev,context=u:object_r:vendor_qmcs_file:s0   wait,check,formattable
/dev/block/bootdevice/by-name/cache                     /cache                 ext4    nosuid,nodev,noatime,barrier=1                                wait,check,continue
/dev/block/bootdevice/by-name/log                       /log                   ext4    noatime,nosuid,nodev,noauto_da_alloc,commit=1,nodelalloc      wait,check,continue,nofail

overlay     /system/product/priv-app                        overlay     ro,lowerdir=/preas/priv-app:/system/product/priv-app                                    check,nofail
overlay     /system/product/app                             overlay     ro,lowerdir=/preas/app:/system/product/app                                              check,nofail
overlay     /system/product/etc/default-permissions         overlay     ro,lowerdir=/preas/oversea/default-permissions:/system/product/etc/default-permissions  check,nofail
overlay     /system/product/etc/permissions                 overlay     ro,lowerdir=/preas/oversea/permissions:/system/product/etc/permissions                  check,nofail
overlay     /system/product/etc/preferred-apps              overlay     ro,lowerdir=/preas/oversea/preferred-apps:/system/product/etc/preferred-apps            check,nofail
overlay     /system/product/etc/default-permissions         overlay     ro,lowerdir=/preas/china/default-permissions:/system/product/etc/default-permissions    check,nofail
overlay     /system/product/etc/permissions                 overlay     ro,lowerdir=/preas/china/permissions:/system/product/etc/permissions                    check,nofail
overlay     /system/product/etc/preferred-apps              overlay     ro,lowerdir=/preas/china/preferred-apps:/system/product/etc/preferred-apps              check,nofail
overlay     /system/product/overlay                         overlay     ro,lowerdir=/preas/oversea/overlay:/system/product/overlay                              check,nofail
overlay     /system/product/etc/security                    overlay     ro,lowerdir=/preas/oversea/security:/system/product/etc/security                        check,nofail
overlay     /system/product/etc/sysconfig                   overlay     ro,lowerdir=/preas/oversea/sysconfig:/system/product/etc/sysconfig                      check,nofail
overlay     /system/product/usr                             overlay     ro,lowerdir=/preas/usr:/system/product/usr                                              check,nofail
overlay     /system/product/etc/sysconfig                   overlay     ro,lowerdir=/preas/china/sysconfig:/system/product/etc/sysconfig                        check,nofail
overlay     /system/product/etc/security                    overlay     ro,lowerdir=/preas/china/security:/system/product/etc/security                          check,nofail
